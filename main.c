#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stddef.h>


char ttt[3][3] = {
        {'-','-','-'},
        {'-','-','-'},
        {'-','-','-'}
    };


void wait();
void ausgabe();
void win_again();
int player1();
int player2();
int zug(int zeile, int spalte);
int checkwin();

int haswon = 0;
int firststart = 1;

int main()
{
    printf("\n\n\n");

    do
    {
        ausgabe();
        player1();
            if (haswon == 1)
			{
				win_again();
			}
        ausgabe();
        player2();
            if (haswon == 2)
			{
				win_again();
			}

    } while (haswon < 1);



   return 0;
} // end of main

void ausgabe()
{
    system("clear");

    if(firststart == 1)
    {
        printf("TTT  Copyright (C) 2020  Curny\n");
        printf("This program comes with ABSOLUTELY NO WARRANTY\n");
        printf("This is free software, and you are welcome to redistribute it\n");
        printf("under certain conditions\n");
        printf("Enter q to quit at any time.\n");
        firststart = 0;
    }

    printf("\n Player 1: X\t\t Player 2: o\n\n");
    printf("\t  | A | B | C |\n");
    printf("\t  +---+---+---+\n");
    for (int i=0;i<3;i++)
    {
        printf("\t%d |", (i+1));
        for (int j=0;j<3;j++)
        {
            printf(" %c |",ttt[i][j]);
        }
        printf("\n");
        printf("\t  +---+---+---+\n");
    }
}

int zug(int zeile, int spalte)
{
    ttt[zeile][spalte] = 'X';


    return 0;

}

int player1()
{
    char angabe[2];    // evtl [3] ?  Testen ob das den Crash fixt
    int valid = 0;
    int zeile = 0;
    int spalte = 0;

  do
  {

    printf("\n Player 1 - your move: ");
    fflush(stdin);
    scanf("%s", &angabe);

    // angabe[1] in integer wandeln
    zeile = zeile * 10 + angabe[1] - '1';

    switch (angabe[0])
    {
        case 'a':
            spalte = 0;
            break;

        case 'b':
            spalte = 1;
            break;

        case 'c':
            spalte = 2;
            break;

        case 'q':
            exit(0);
            break;

        default:
            printf("Wrong input!");
            angabe[0] = "";
            continue;

    }


     if (ttt[zeile][spalte] == '-')
    {
        ttt[zeile][spalte] = 'X';
        haswon = checkwin();
        valid = 1;
    }
    else
    {
        printf("\n Invalid move, choose another field!\n");
        fflush(stdin);
		zeile = 0;
		spalte = 0;
        valid = 0;
    }
  } while (valid != 1);
    return 0;
}

int player2()
{
    char angabe[2];   // evtl [3]
    int valid = 0;
    int zeile = 0;
    int spalte = 0;

do
{

    printf("\n Player 2 - your move: ");
    fflush(stdin);
    scanf("%s", &angabe);

    // angabe[1] in integer wandeln
    zeile = zeile * 10 + angabe[1] - '1';

    switch (angabe[0])
    {
        case 'a':
            spalte = 0;
            break;

        case 'b':
            spalte = 1;
            break;

        case 'c':
            spalte = 2;
            break;

        case 'q':
            exit(0);
            break;

        default:
            printf("Wrong input!");
            angabe[0] = "";
            continue;

    }

    if (ttt[zeile][spalte] == '-')
    {
        ttt[zeile][spalte] = 'o';
        haswon = checkwin();
        valid = 1;
    }
    else
    {
        printf("\n Invalid move, choose another field!\n");
        fflush(stdin);
        zeile = 0;
		spalte = 0;
        valid = 0;
    }
    } while (valid != 1);

    return 0;
}

int checkwin()
{
    // return 1 for win
    // return 0 for not win
    char again;

    char a1 = ttt[0][0];
    char a2 = ttt[1][0];
    char a3 = ttt[2][0];

    char b1 = ttt[0][1];
    char b2 = ttt[1][1];
    char b3 = ttt[2][1];

    char c1 = ttt[0][2];
    char c2 = ttt[1][2];
    char c3 = ttt[2][2];

    // check if player 1-X has won
    if (a1 == 'X' && a2 == 'X' && a3 == 'X')
    {
        return 1;
    }
    else if (b1 == 'X' && b2 == 'X' && b3 == 'X')
    {
        return 1;
    }
    else if (c1 == 'X' && c2 == 'X' && c3 == 'X')
    {
        return 1;
    }
    else if (a1 == 'X' && b2 == 'X' && c3 == 'X')
    {
        return 1;
    }
    else if (a2 == 'X' && b2 == 'X' && c2 == 'X')
    {
        return 1;
    }
    else if (a3 == 'X' && b3 == 'X' && c3 == 'X')
    {
        return 1;
    }
    else if (a3 == 'X' && b2 == 'X' && c1 == 'X')
    {
        return 1;
    }
    else if (a1 == 'X' && b1 == 'X' && c1 == 'X')
    {
        return 1;
    }


    // check if player 2-o has won
    if (a1 == 'o' && a2 == 'o' && a3 == 'o')
    {
        return 2;
    }
    else if (b1 == 'o' && b2 == 'o' && b3 == 'o')
    {
        return 2;
    }
    else if (c1 == 'o' && c2 == 'o' && c3 == 'o')
    {
        return 2;
    }
    else if (a1 == 'o' && b2 == 'o' && c3 == 'o')
    {
        return 2;
    }
    else if (a2 == 'o' && b2 == 'o' && c2 == 'o')
    {
        return 2;
    }
    else if (a3 == 'o' && b3 == 'o' && c3 == 'o')
    {
        return 2;
    }
    else if (a3 == 'o' && b2 == 'o' && c1 == 'o')
    {
        return 2;
    }
    else if (a1 == 'o' && b1 == 'o' && c1 == 'o')
    {
        return 2;
    }

    if (a1 != '-' && a2 != '-' && a3 != '-' && b1 != '-' && b2 != '-' && b3 != '-' && c1 != '-' && c2 != '-' && c3 != '-')
    {
        printf("\n\n *** No more moves possible. Start over? (y/n) ***");

        fflush(stdin);
        scanf("%c", &again);

        if (again == "n")
        {
            system("clear");
            exit(0);
        }
        else
        {
            for (int i=0; i<3; i++)
            {
                for (int j=0; j<3; j++)
                {
                    ttt[i][j] = '-';
                }
            }

        }

    }


    return 0;
}

void wait()
{
	fflush(stdin);
	getchar();
}

void win_again()
{
	char again;

	printf("\n");
	printf("\n\n***************************");
	printf("\n******** PLAYER  %d ********", haswon);
	printf("\n********  YOU WIN  ********");
	printf("\n***************************\n\n\n");
	printf("Play again? (y/n)");
	fflush(stdin);
	getchar();
	scanf("%c", &again);

	if (again == 'y')
	{
		ttt[0][0] = '-';
		ttt[1][0] = '-';
		ttt[2][0] = '-';

		ttt[0][1] = '-';
		ttt[1][1] = '-';
		ttt[2][1] = '-';

		ttt[0][2] = '-';
		ttt[1][2] = '-';
		ttt[2][2] = '-';
		haswon = 0;
	}
	else
	{
		exit(0);
	}

}
